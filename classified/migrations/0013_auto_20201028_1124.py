# Generated by Django 3.1.2 on 2020-10-28 11:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('classified', '0012_sale_sended'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sale',
            name='payed_at',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Дата продажи'),
        ),
    ]
