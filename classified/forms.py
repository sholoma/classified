from django import forms

class PaymentForm(forms.Form):
    work = forms.IntegerField()
    email = forms.EmailField()