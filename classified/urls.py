"""classified URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from classified.views import (
    WorksView,
    work,
    page,
    success,
    fail,
    pay,
    payment
)
from ckeditor_uploader import views
from django.views.static import serve
import os
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.cache import never_cache
admin.site.site_header = 'Поддержка mail@sholomitcky.com , тел +7 920 0210884'
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^ckeditor/upload/', login_required(views.upload), name='ckeditor_upload'),
    url(r'^ckeditor/browse/', never_cache(staff_member_required(views.browse)), name='ckeditor_browse'),
    url(r'^static/(?P<path>.*)', serve, {'document_root': os.path.join(os.path.dirname(__file__), 'static')}),
    url(r'^media/(?P<path>.*)', serve, {'document_root': os.path.join(os.path.dirname(__file__), 'media')}),
    url(r'^work/(?P<pk>[0-9]+)/$', work, name='work'),
    path('admin/', admin.site.urls),
    url(r'^catalog/$', WorksView.as_view(), name='catalog'),
    url(r'^success/$', success, name='success'),
    url(r'^fail/$', fail, name='fail'),
    url(r'^payment/$', payment, name='payment'),
    url(r'^pay/$', pay, name='pay'),
    url(r'^agreement/$',  TemplateView.as_view(template_name="agreement.jinja2"), name='agreement'),
    url(r'^refund/$',  TemplateView.as_view(template_name="refund.jinja2"), name='refund'),
    path('robots.txt', TemplateView.as_view(template_name="robots.txt", content_type='text/plain')),
    url(r'^$', page, name='mainpage', kwargs={'slug': 'mainpage'}),
    url(r'^(?P<slug>.*)', page, name='page'),


]
