from .models import (
    Subject,
    WorkType,
    Work,
    Page,
    Settings
)
from django.core.cache import cache
from django.db.models import Count


def suject_works_count(request):
    key = 'suject_works_count'
    works_count = cache.get(key)
    if not works_count:
        works_count = Work.objects.count()
        cache.set(key, works_count, 30)
    return {'suject_works_count': works_count}


def settings(request):
    return {'settings': Settings.objects.first()}


def all_subjects(request):
    return {'all_subjects': Subject.objects.annotate(works_count=Count('works')).order_by('name')}


def all_types(request):
    return {'all_types': WorkType.objects.all()}


def pages_menu(request):
    return {'all_pages': Page.objects.filter(menu=True).only('name', 'slug')}
