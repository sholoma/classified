import os
ADMINS = (
    ('Sholomitcky S.A.', 'mail@sholomitcky.com'),
)

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '0$^t=s=ey3(jka=@ft5!nt%kak30#*g6xq&)!ja+4p@9$g&dpi'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*',  '127.0.0.1']


# Application definition

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # other finders..
    'compressor.finders.CompressorFinder',
)

INSTALLED_APPS = [
    #'admin_tools',
    #'admin_tools.theming',

    #'admin_tools.dashboard',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_cleanup.apps.CleanupConfig',
    'classified',
    'bootstrap4',
    "compressor",
    'django_filters',
    'cacheops',
    "moneta",
    'ckeditor',
    'ckeditor_uploader',
    # 'dal',
    # 'dal_select2',
]

DATA_UPLOAD_MAX_MEMORY_SIZE = 99999999

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    #'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    #'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

CORS_ORIGIN_ALLOW_ALL = True

CKEDITOR_UPLOAD_PATH = "uploads/"

MONETA = {
    "basic_config": {
        "account_id": "78903163",
        "account_username": "+79063521545",
        "account_password": "R196AS+",
        "payment_system_name": "plastic",
        "currency": "RUB",
        "account_code": "12345",
        "test_mode": 0,
        "demo_mode": 0
    }
}


CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'full',
        'pasteFilter': 'plain-text'
    },
}

ROOT_URLCONF = 'classified.urls'


TEMPLATES = [
    {
        "BACKEND": "django.template.backends.jinja2.Jinja2",
        "DIRS": [os.path.join(os.path.dirname(__file__), 'jinja2').replace('\\', '/'), ],
        "APP_DIRS": False,
        "OPTIONS": {
            'context_processors': [
                'django.contrib.messages.context_processors.messages',
                'classified.context_processors.all_subjects',
                'classified.context_processors.all_types',
                'classified.context_processors.suject_works_count',
                'classified.context_processors.pages_menu',
                'classified.context_processors.settings'
            ],
            'environment': 'classified.jinja2.environment',
            #"match_extension": ".jinja2",
            "auto_reload":True,
            "cache_size":0,
            "extensions": [
                # The default extensions, you should include them
                # if you are overwritting the settings.
                "jinja2.ext.do",
                "jinja2.ext.loopcontrols",
                "jinja2.ext.with_",
                "jinja2.ext.i18n",
                "jinja2.ext.autoescape",
                'compressor.contrib.jinja2ext.CompressorExtension',
                # "django_jinja.builtins.extensions.CsrfExtension",
                "django_jinja.builtins.extensions.CacheExtension",
                "django_jinja.builtins.extensions.TimezoneExtension",
                #"django_jinja.builtins.extensions.UrlsExtension",
                "django_jinja.builtins.extensions.StaticFilesExtension",
                "django_jinja.builtins.extensions.DjangoFiltersExtension",
            ],

        }
    },
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        "DIRS": [os.path.join(os.path.dirname(__file__),)],
        'APP_DIRS': False,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'loaders': [
                ('django.template.loaders.cached.Loader', [
                    'django.template.loaders.filesystem.Loader',
                    'django.template.loaders.app_directories.Loader',
                    'admin_tools.template_loaders.Loader',
                ]),
            ],
        },
    },
]

WSGI_APPLICATION = 'classified.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'classified',
        #'NAME': 'test',
        'USER': 'postgres',
        'PASSWORD': 'fanatubuntu',
        'HOST': '127.0.0.1',
        'PORT': '5432',
        'CONN_MAX_AGE': 60,
        'DEFAULT_TIMEOUT': 360,
        'OPTIONS': {
            'sslmode': 'disable',
        },
    }
}
# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(os.path.dirname(__file__), 'static').replace('\\', '/')
MEDIA_ROOT = os.path.join(os.path.dirname(__file__), 'media').replace('\\', '/')
MEDIA_URL = '/media/'
try:
    from classified.local import *
except:
    pass

CELERY_RESULT_BACKEND = 'django-db'
CELERY_TASK_RESULT_EXPIRES = 360000
#CELERY_BROKER_URL = 'redis://127.0.0.1:6379/0'
#CELERY_RESULT_BACKEND = "redis://127.0.0.1:6379/0"

EMAIL_BACKEND = 'django_smtp_ssl.SSLEmailBackend'

EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_PORT = 465
EMAIL_HOST_USER = 'noreply@ukcevrica.ru'
EMAIL_HOST_PASSWORD = 'fanatubuntu'
EMAIL_USE_TLS = True

DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
SERVER_EMAIL = DEFAULT_FROM_EMAIL

ADMIN_TOOLS_INDEX_DASHBOARD = 'classified.dashboard.CustomIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'classified.dashboard.CustomAppIndexDashboard'

DATETIME_FORMAT = '%d.%m.%Y %H:%M:%S'
LOGIN_URL = '/login/'


if not DEBUG:
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'filters': {
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse'
            }
        },
        "formatters": {
            "rq_console": {
                "format": "%(asctime)s %(message)s",
                "datefmt": "%H:%M:%S",
            },
        },
        'handlers': {
            'mail_admins': {
                'level': 'ERROR',
                #  'filters': ['require_debug_false'],
                'class': 'django.utils.log.AdminEmailHandler'
            },
        },
        'loggers': {
            'django.request': {
                'handlers': ['mail_admins',],
                'level': 'ERROR',
                'propagate': True,
            },
        }
    }

CACHEOPS_REDIS = {
    'host': 'localhost', # redis-server is on same machine
    'port': 6379,        # default redis port
    'db': 2,             # SELECT non-default redis database
    'socket_timeout': 3
}

CACHEOPS = {
    'app.*': {'ops': 'all', 'timeout': 300},
}

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}


UNITPAY_PUBLIC = '339701-24882'
UNITPAY_SECRET = 'b1a4866285b737b8fe52c993c4e4d453'