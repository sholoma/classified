from django.core.management.base import BaseCommand
from classified.models import Sale, Settings
from classified.settings import DEFAULT_FROM_EMAIL
from datetime import date, timedelta
from django.core.mail import EmailMessage
from email.mime.application import MIMEApplication
import os
import magic

class Command(BaseCommand):

    def handle(self, *args, **options):
        settings = Settings.objects.first()
        subject = settings.email_subject
        text = settings.email_text
        for sale in Sale.objects.filter(sended=False).exclude(payed_at__isnull=True):
            sale.sended = True
            sale.save()
            msg = EmailMessage(
                subject=subject,
                body=text,
                from_email=DEFAULT_FROM_EMAIL,
                to=[sale.email],
            )
            for ma in sale.work.files.all():
                f = ma.file.path
                mime = magic.Magic(mime=True)
                filename = os.path.basename(f)
                try:
                    file_data = open(f, 'rb').read()
                except:
                    continue
                attach_file = MIMEApplication(file_data, _subtype=mime.from_file(f))
                attach_file.add_header('Content-Disposition', 'attachment; filename="%s"' % (filename))
                msg.attach(attach_file)
            msg.content_subtype = "html"
            msg.send()
