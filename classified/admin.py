from django.contrib import admin
from .models import (
    Subject,
    WorkType,
    Work,
    WorkFiles,
    Page,
    Sale,
    Settings
)


@admin.register(Settings)
class SettingsAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return not Settings.objects.all().exists()

    def has_delete_permission(self, request, obj=None):
        return False

    fieldsets = (
        (None, {
            'fields': ('sitename',)
        }),
        ('Контакты', {
            'fields': ('name', 'phone', 'vk', 'email')
        }),
        ('Письмо покупателю', {
            'fields': ('email_subject', 'email_text')
        }),
    )

@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    list_display = ['email', 'price', 'work', 'payed_at']
    date_hierarchy = 'payed_at'
    search_fields = ['email']
    exclude = ['sended']

    def has_add_permission(self, request):
        return False

    def get_queryset(self, request):
        return Sale.objects.exclude(payed_at__isnull=True)



@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    list_display = ['name', 'menu', 'order', 'show_sidebar', 'show_url']
    list_editable = ['menu', 'order', 'show_sidebar']
    prepopulated_fields = {"slug": ("name",),}

    def has_delete_permission(self, request, obj=None):
        if not obj:
            return True
        if obj.slug == 'mainpage':
            return False
        return True

    def get_readonly_fields(self, request, obj=None):
        if not obj:
            return []
        if obj.slug == 'mainpage':
            return ['slug']
        return []

    def get_prepopulated_fields(self, request, obj=None):
        if not obj:
            return {}
        if obj.slug == 'mainpage':
            return {}
        return self.prepopulated_fields

@admin.register(Subject)
class SubjectAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']


@admin.register(WorkType)
class WorkTypeAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']


class WorkFilesAdmin(admin.TabularInline):
    model = WorkFiles
    extra = 0


@admin.register(Work)
class WorkAdmin(admin.ModelAdmin):
    list_display = ['name', 'subject', 'worktype', 'price']
    search_fields = ['name']
    list_filter = ['subject', 'worktype']
    inlines = [WorkFilesAdmin]