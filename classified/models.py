from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
import uuid
import os
from django.urls import reverse
from django.utils.html import mark_safe


def get_file_path(instance, filename):
    return os.path.join('%s' % uuid.uuid4(), filename)


class Settings(models.Model):
    class Meta:
        verbose_name = 'Настройки сайта'
        verbose_name_plural = u'Настройки сайта'
    sitename = models.CharField(u'Название сайта', max_length=100, default='Учебно-консультативный центр Эврика')
    name = models.CharField(u'Имя', max_length=30, default='Евгения Смирнова')
    phone = models.CharField(u'Телефон', max_length=30, default='+79063521545')
    vk = models.URLField(u'Ссылка на VK', default='https://vk.com/zhene4ka82')
    email = models.EmailField(u'E-mail', default='mail@mail.ru')
    email_subject = models.CharField(u'Тема письма', max_length=200, default=u'Ваш заказ на ukcevrica.ru')
    email_text = models.TextField(u'Содержание письма', default=u'Во вложении файлы вашего заказа')

    def __str__(self):
        return u'настройки'


class Subject(models.Model):
    class Meta:
        verbose_name = u'Предмет'
        verbose_name_plural = 'Предметы'
        ordering = ['name']
    name = models.CharField(u'Название предмета', max_length=100, unique=True)

    def __str__(self):
        return self.name


class WorkType(models.Model):
    class Meta:
        verbose_name = u'Тип работы'
        verbose_name_plural = u'Типы работ'
        ordering = ['name']
    name = models.CharField(u'Название', max_length=100, unique=True)

    def __str__(self):
        return self.name


class Work(models.Model):
    class Meta:
        verbose_name_plural = u'Работы'
        verbose_name = u'Работа'
        ordering = ['name', 'subject']
    name = models.CharField(u'Тема', max_length=250)
    subject = models.ForeignKey(Subject, verbose_name=u'Предмет', related_name=u'works', on_delete=models.CASCADE)
    worktype = models.ForeignKey(WorkType, verbose_name=u'Тип работы', related_name=u'works', on_delete=models.CASCADE)
    pages = models.PositiveIntegerField(u'Количество страниц', blank=True, null=True)
    date = models.CharField(u'Год работы', blank=True, null=True, max_length=20)
    originality = models.CharField(u'Оригинальность', max_length=100, blank=True, null=True)
    content = RichTextUploadingField(u'Содержание')
    literature = RichTextUploadingField(u'Литература', blank=True, null=True)
    about = RichTextUploadingField(u'О работе', blank=True, null=True)
    price = models.SmallIntegerField(u'Цена')

    def send_to_email(self):
        settings = Settings.objects.first()

    def __str__(self):
        return self.name


class WorkFiles(models.Model):
    class Meta:
        verbose_name = u'Файл работы'
        verbose_name_plural = u'файлы работы'
    work = models.ForeignKey(Work, verbose_name=u'Работа', related_name='files', on_delete=models.CASCADE)
    file = models.FileField(u'Файл', upload_to=get_file_path)

    def __str__(self):
        return self.file.name


class Page(models.Model):
    class Meta:
        verbose_name = 'Страница'
        verbose_name_plural = 'Страницы'
        ordering = ['order', 'name']
    name = models.CharField(u'Название', help_text='Заголовок страницы', max_length=200)
    slug = models.SlugField(u'Ссылка', unique=True)
    content = RichTextUploadingField(u'Содержание')
    order = models.SmallIntegerField(u'Порядок в меню', default=1)
    menu = models.BooleanField(u'Показывать в меню', default=True)
    show_sidebar = models.BooleanField(u'Показывать боковое меню', default=True)

    def show_url(self):
        url = reverse('page', args=[self.slug])
        if self.slug == 'mainpage':
            url = '/'
        return mark_safe('<a target="_blank" href="{url}">{url}</a>'.format(url=url))

    show_url.short_description = 'Ссылка на страницу'

    def __str__(self):
        return self.name


class Sale(models.Model):
    class Meta:
        verbose_name = u'Продажа'
        verbose_name_plural = 'Продажи'
        ordering = ['-payed_at']
    payed_at = models.DateTimeField(u'Дата продажи', blank=True, null=True)
    work = models.ForeignKey(Work, verbose_name=u'Работа', blank=True, null=True, on_delete=models.SET_NULL)
    email = models.EmailField(u'E-mail покупателя')
    price = models.IntegerField(u'Цена')
    sended = models.BooleanField(u'отправлено на почту', default=False)

    def __str__(self):
        return u'Продажа %s на сумму %s' % (self.work or u'удалённая работа', self.price)
