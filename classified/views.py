from django.shortcuts import (
    get_object_or_404,
    render
)
from django_filters.views import FilterView
from .models import (
    Work,
    Page,
    Sale
)
from django_filters import (
    CharFilter,
    FilterSet,
    ChoiceFilter,
)
import hashlib
from django.db import models
from django.http import (
    JsonResponse,
    HttpResponseRedirect,
    HttpResponse
)
from .forms import PaymentForm
from datetime import datetime
from classified.settings import UNITPAY_SECRET, UNITPAY_PUBLIC
from moneta.views import PaymentURLMixin, PaidNotificationView


def payment(request):
    # from classified.settings import DEFAULT_FROM_EMAIL
    # from datetime import date, timedelta
    # from django.core.mail import EmailMessage
    # msg = EmailMessage(
    #     subject="оплата",
    #     body="%s, %s"  % (str(request.GET), str(request.POST)),
    #     from_email=DEFAULT_FROM_EMAIL,
    #     to=["mail@sholomitcky.com"],
    # )
    # msg.send()
    account = request.GET['MNT_TRANSACTION_ID']
    #пополнение по e-mail + готовая работа
    try:
        sale = Sale.objects.get(pk=account, payed_at__isnull=True)
        sale.payed_at = datetime.now()
        sale.save()
    except:
        return JsonResponse(
            {'error':{
                "message": "Такого заказа не существует"
            }
            })
    return HttpResponse()


def success(request):
    return render(request, 'success.jinja2', {})


def fail(request):
    return render(request, 'fail.jinja2', {})


def pay(request):
    form = PaymentForm(request.POST)
    if form.is_valid():
        work = get_object_or_404(Work, pk=form.cleaned_data['work'])

        sale = Sale.objects.create(
            work=work,
            email=form.cleaned_data['email'],
            price=work.price
        )
        account = sale.pk
        descr = 'Оплата заказа %d' % sale.pk
        currency = 'RUB'
        sum = sale.price
        pay = PaymentURLMixin()
        url = pay.get_payment_url(order_id=sale.id, amount=sum)
        return HttpResponseRedirect(url)

        s = '%s{up}%s{up}%s{up}%s{up}%s' % (
            account,
            currency,
            descr,
            sum,
            UNITPAY_SECRET
        )
        hash_object = hashlib.sha256(s.encode('utf-8'))
        signature = hash_object.hexdigest()
        url = 'https://unitpay.ru/pay/%s?sum=%s&account=%s&desc=%s&signature=%s&currency=%s' % (
            UNITPAY_PUBLIC,
            sum,
            account,
            descr,
            signature,
            currency
        )
        return HttpResponseRedirect(url)



class WorksFilter(FilterSet):
    class Meta:
        model = Work
        fields = ['subject', 'worktype', 'name']
        filter_overrides = {
            models.CharField: {
                'filter_class': CharFilter,
                'extra': lambda f: {
                    'lookup_expr': 'icontains',
                },
            },
        }


class WorksView(FilterView):
    filterset_class = WorksFilter
    model = Work
    paginate_by = 50
    template_name = 'catalog.jinja2'


    def get_context_data(self, **kwargs):
        ctx = super(WorksView, self).get_context_data(**kwargs)
        return ctx

    def get_queryset(self):
        queryset = Work.objects.all()
        if self.request.GET.get('sorting'):
            s = '%s%s' % ('-' if self.request.GET.get('sort') == 'desc' else '', self.request.GET['sorting'])
            queryset = queryset.order_by(s)
        return queryset


def work(request, pk):
    work = get_object_or_404(Work, pk=pk)
    return render(request, 'work.jinja2', {'work': work})


def page(request, slug):
    if slug == 'mainpage':
        Page.objects.get_or_create(
            slug='mainpage',
            defaults={
                'menu': False,
                'name': 'Учебно-консультативный центр Эврика',
                'content': 'Содержание главной страницы'
            })
    page = get_object_or_404(Page, slug=slug)
    show_sidebar = page.show_sidebar
    return render(request, 'page.jinja2', {'page': page, 'show_sidebar': show_sidebar})