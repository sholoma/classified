from math import ceil
from django.contrib.humanize.templatetags.humanize import intcomma
from django.urls import reverse
from jinja2 import Environment
from django.db.models import Q
from django.utils.translation import ugettext
from datetime import date
from json import loads
from bootstrap4.templatetags.bootstrap4 import (
    bootstrap_form,
    bootstrap_field,
    bootstrap_formset
)


def myhumanize(val):
    try:
        if val == int(val):
            val = int(val)
        return intcomma(val).replace(',', '.')
    except:
        return val

def myhumanize_float(val):
    return intcomma(val)

def verbose_name(obj, name):
    try:
        return obj._meta.get_field(name).verbose_name.title()
    except:
        return name

def get_object_value(obj, name, request=None):
    if callable(getattr(obj, name)):
        f = getattr(obj, name)
        if '_display' in name:
            try:
                return f()
            except:
                pass
            try:
                return f(request)
            except:
                pass
        else:
            try:
                return f(obj, request)
            except:
                pass
            try:
                return f(request)
            except:
                pass
            try:
                return f()
            except:
                pass
    else:
        attr = getattr(obj, name)
        if type(attr) == date:
            return attr.strftime('%d.%m.%Y')
        return attr


def params(request, args=None, dels=[]):
    params = request.GET.copy()
    if args:
        for key, value in args.items():
            #   if key not in params:
            params[key] = value
    for d in dels:
        try:
            del params[d]
        except:
            pass
    if len(params) == 0:
        return ''
    elif '?%s' % params.urlencode() == '?page=1':
        return request.path
    else:
        return '?%s' % params.urlencode()


def prepare_search_request(request):
    params = request.GET.copy()
    try:
        del params['page']
    except:
        pass
    if len(params) == 0:
        return None
    else:
        return '?%s' % params.urlencode()


def split_name(txt):
    sp = txt.split()
    if len(sp) == 1:
        return txt
    return '%s <span>%s</span>' % (
        ' '.join(sp[:-1]),
        sp[-1]
    )

def environment(**options):
    env = Environment(**options)
    env.globals.update({
        'get_object_value': get_object_value,
        'getattr': getattr,
        'verbose_name': verbose_name,
        'url': reverse,
        'ceil': ceil,
        'params': params,
        'split_name': split_name,
        'Q': Q,
        'jloads': loads,
        'myhumanize': myhumanize,
        'myhumanize_float': myhumanize_float,
        'prepare_search_request': prepare_search_request,
        'bootstrap_formset': bootstrap_formset,
        'bootstrap_field': bootstrap_field,
        'bootstrap_form': bootstrap_form
    })
    return env
